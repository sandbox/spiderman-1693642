Description
-----------

This module provides a "Percentage Calculation" component to webform-enabled
content types for Drupal 7. The content editor can add this component to their
webform, and select 2 other components (already added to the form) which are
the numerator and denominator of the built-in percentage calculation function.

The module then displays a dynamically updated percentage value while the data
entry user is filling in the form. This is achieved by adding some simple
(soon-to-be themeable) div-set markup + a hidden input field, both of which are
updated whenever the selected numerator/denominator components change.

I have borrowed heavily from http://drupal.org/project/webform_calculator to
write this (unfinished) module, after failing to make it work to meet my
immediate (client's) needs. My hope would be to merge with that project,
providing a generalized calculated-value component for the excellent Webform
module, with some display-side niceties for the js-loving set. If you're
interested in helping this effort, see Development notes below.

Requirements
------------
Drupal 7.x
Webform 3.x 
jQuery Update 2.x-dev (jQuery 1.7)

Installation
------------

1. Copy the module into the appropriate sites/[sitename]/modules directory.

2. Enable the module on the site.

3. Create or edit an existing Webform-enabled node

4. Create at least 2 textfield or number form components, and then add a
"Percentage Calculation" component to the form.

5. On the component configuration screen, specify the numerator component,
denominator component, and precision.

6. When viewing the Webform, you will see a "Percentage" labelled value
(initially 0%) which updates automatically whenever you enter non-zero values
in the 2 selected "numerator" and "denominator" components.

Development (in progress)
-------------------------

My primary focus has been on the dynamic display of the calculated value, and
to be frank this is my first in-depth study of JQuery in Drupal, an area in
which I'm a complete n00b. I've tried to keep the module code simple,
essentially implementing a 2-part html component, and all the corresponding
webform-component hooks (so it can be displayed, exported, etc). 
The hidden input field ensures that the value of the calculation is submitted
properly with the form, thus enabling all the niceties of the webform hooks.

Re: Webform_calculator-

My approach here has been to simplify the calculation side logic, since in my
immediate case I can assume >90% of the forms being built will only require a
simple (numerator / denominator * 100), rounded to a given precision. The
webform_calculator module includes an interesting "evalMath" PHP class which
does some nice generalized (and supposedly parameterized) "safe" calculation of
arbitrary expressions.

I would *love* to see this working, but my initial efforts to make
webform_calculator work met with two showstopper issues:

* webform_calculator targets a use-case where the calculation only needs to be
  shown post-submission. The end-user never sees the calculation happening. In
  my case, I want to display the calculation so the end-user can confirm/verify
  the numbers they've entered, and get some immediate feedback- these same
  users will be looking at trends in this same data later on as well.

* I couldn't manage to get the token-replacement mechanism built-in to
  webform_calculator to work for me. This turned out to be a side-effect of an
  immutable and very specific naming scheme for our form components. Ultimately
  it seems like integration with the excellent Token module is in order, and if
  I have a chance, I'll gladly patch that in :)

I have tried to leave room where possible (and obvious) for the option of
providing other calculations, but this effort is probably non-trivial- the
evalMath.php library could certainly be a useful basis for this, at least.
Combined with Token integration and some better javascript-fu, this could
turn into a rather powerful module.
