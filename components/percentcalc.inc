<?php

/**
 * @file
 * Webform module markup component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_percentcalc() {
  return array(
    'name' => 'Calculated Percentage',
    'form_key' => '_percentage',
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(),
  );
}

/**
 * Return an array of possible textfield + numeric fields on this webform node.
 */
function _webform_percentcalc_possible_components($nid) {
  $options = array();
  // Primarily consider textfields and number fields.
  $types = array('number', 'textfield');
  $node = node_load($nid);
  $components = $node->webform['components'];
  foreach ($components as $component) {
    if (in_array($component['type'], $types)) {
      $key = $component['form_key'];
      $options[$key] = $component['name'];
    }
  }
  return $options;
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_percentcalc($component) {
  $form = array();
  // Here provide options for possible textfield inputs..
  $options = _webform_percentcalc_possible_components($component['nid']);
  $form['display']['field_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Label placed to the left of the calculated value'),
    '#default_value' => $component['extra']['field_prefix'],
    '#description' => t('Examples: $, #, -.'),
    '#size' => 20,
    '#maxlength' => 127,
    '#weight' => 1.1,
    '#parents' => array('extra', 'field_prefix'),
  );
  $form['display']['field_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Label placed to the right of the textfield'),
    '#default_value' => $component['extra']['field_suffix'],
    '#description' => t('Examples: lb, kg, %.'),
    '#size' => 20,
    '#maxlength' => 127,
    '#weight' => 1.2,
    '#parents' => array('extra', 'field_suffix'),
  );

  // Configure the divide by zero error message.
  $form['extra']['undefined_msg'] = array(
    '#type' => 'textfield',
    '#title' => t('Displayed auto-calculation value for an undefined, divide by zero result'),
    '#default_value' => !empty($component['extra']['undefined_msg']) ? $component['extra']['undefined_msg'] : 'Undefined (divide by zero error)',
    '#description' => t('This value gets displayed in the webform when denominator is zero. In the database, the auto-calculated value is saved as "NaN", Not a Number.'),
    '#size' => 20,
    '#maxlength' => 127,
  );
  $form['extra']['numerator'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Numerator'),
    '#default_value' => !empty($component['extra']['numerator']) ? $component['extra']['numerator'] : NULL,
    '#description' => t('Numerator component of the percentage calculation'),
    '#element_validate' => array('_webform_edit_percentcalc_validate'),
  );
  $form['extra']['denominator'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Denominator'),
    '#default_value' => !empty($component['extra']['denominator']) ? $component['extra']['denominator'] : NULL,
    '#description' => t('Denominator component of the percentage calculation'),
    '#element_validate' => array('_webform_edit_percentcalc_validate'),
  );
  $form['extra']['precision'] = array(
    '#type' => 'textfield',
    '#title' => t('Precision'),
    '#size' => 3,
    '#default_value' => !empty($component['extra']['precision']) ? $component['extra']['precision'] : NULL,
    '#description' => t('Number of significant digits (after the decimal point).'),
  );
  $form['extra']['calculation'] = array(
    '#type' => 'select',
    '#options' => array(
      'percentage' => 'Percentage (num / denom) * 100',
      'division' => 'Simple Division (num / denom)',
    ),
    '#title' => t('Calculation'),
    '#default_value' => $component['value'],
    '#description' => t('The calculation function to apply (currently only percentage)'),
    '#element_validate' => array('_webform_edit_percentcalc_validate'),
  );

  return $form;
}

/**
 * Element validate handler; Set the precision value.
 */
function _webform_edit_percentcalc_validate($form, &$form_state) {
  if (isset($form_state['values']['value']) && is_array($form_state['values']['value'])) {
    $form_state['values']['extra']['numerator'] = $form_state['values']['value']['numerator'];
    $form_state['values']['extra']['denominator'] = $form_state['values']['value']['denominator'];
    $form_state['values']['extra']['precision'] = $form_state['values']['value']['precision'];
    $form_state['values']['value'] = $form_state['values']['value']['value'];
  }
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_percentcalc($component, $value = NULL, $filter = TRUE) {
  $num_key = $component['extra']['numerator'];
  $den_key = $component['extra']['denominator'];

  $value = 0;
  $element['#weight'] = $component['weight'];
  $settings =
    array(
        $component['cid'] => array(
          'percentage' => _webform_percentcalc_convert_key_to_percentcalc_label($component['form_key']),
          'hidden_value' => _webform_percentcalc_convert_key_to_hidden($component['form_key']),
          'numerator' => _webform_percentcalc_convert_key_to_input($num_key),
          'denominator' => _webform_percentcalc_convert_key_to_input($den_key),
          'precision' => $component['extra']['precision'],
          'suffix' => isset($component['extra']['field_suffix']) ? $component['extra']['field_suffix'] : '',
          'undefined_msg' => isset($component['extra']['undefined_msg']) ? $component['extra']['undefined_msg'] : 'NaN',
          'calculation' => $component['extra']['calculation']
        )
    );
  $element['#attached']['js'][] = array(
    'data' => array('webform_percentcalc' => $settings),
    'type' => 'setting',
  );
  $element['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'webform_percentcalc') . '/webform_percentcalc.js',
    'type' => 'file',
  );
  $element['markup'] = array(
    '#type' => 'markup',
    '#title' => $filter ? NULL : $component['name'],
    '#weight' => $component['weight'],
    // TODO: make this theme-able
    '#markup' => '<span class="webform-percentcalc-value">' . $value . '</span>',
    '#theme_wrappers' => array('webform_element'),
    '#field_prefix' => empty($component['extra']['field_prefix']) ? NULL : ($filter ? _webform_filter_xss($component['extra']['field_prefix']) : $component['extra']['field_prefix']),
    '#field_suffix' => empty($component['extra']['field_suffix']) ? NULL : ($filter ? _webform_filter_xss($component['extra']['field_suffix']) : $component['extra']['field_suffix']),
    '#webform_component' => $component,
  );
  $element['hidden'] = array(
    '#type' => 'hidden',
    '#default_value' => $value,
    '#weight' => $component['weight'] + 1,
  );

  return $element;
}

/**
 * Implements _webform_submit_component().
 *
 * Saves the re-calculated value
 */
function _webform_submit_percentcalc($component, $value) {
  // TODO: see if there is a way to bring this logic to this level, rather than
  // the submission_presave hook we have now
  // if ($value['hidden'] == 0) { # JS may be disabled, recalculate here.
  $value[0] = $value['hidden'];
  unset($value['hidden']);
  return $value;
}


/**
 * Implements _webform_display_component().
 */
function _webform_display_percentcalc($component, $value, $format = 'html') {
  // Get the field suffix if it is set by the component. If the value is NaN,
  // don't display the suffix.
  $field_suffix = isset($component['extra']['field_suffix']) ? $component['extra']['field_suffix'] : '';
  $field_suffix = (isset($value[0]) && $value[0] != 'NaN') ? $field_suffix : '';

  // Get the component's input value if it is set by the component. If the value
  // is NaN, display an 'undefined' message.
  $display_value = isset($value[0]) ? $value[0] : '';
  $display_value = ($value[0] == 'NaN' && isset($component['extra']['undefined_msg'])) ? $component['extra']['undefined_msg'] : $display_value;

  $display = array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#field_prefix' => $component['extra']['field_prefix'],
    '#field_suffix' => $field_suffix,
    '#component' => $component,
    '#format' => $format,
    '#value' => $display_value,
  );
  return $display;
}

/**
 * Implements_webform_table_component().
 */
function _webform_table_percentcalc($component, $value) {
  return check_plain($value[0]);
}

/**
 * Implements_webform_csv_headers_component().
 */
function _webform_csv_headers_percentcalc($component, $export_options) {
  $header[0] = array('');
  $header[1] = array('');
  $header[2] = array($component['name']);
  return $header;
}

/**
 * Format the output of data for this component.
 */
function theme_webform_display_percentcalc($variables) {
  $element = $variables['element'];
  $value = $element['#format'] == 'html' ? check_plain($element['#value']) : $element['#value'];
  $prefix = $element['#format'] == 'html' ? '' : $element['#field_prefix'];
  $suffix = ($element['#format'] != 'html' && isset($value)) ? $element['#field_suffix'] : '';
  return $value !== '' ? ($prefix . $value . $suffix) : ' ';
}


/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_percentcalc($component, $export_options, $value) {
  return check_plain($value[0]);
}

/**
 * Convert a component_key into a jquery selector for the input field.
 *
 * Turn the component key for a field into a jquery selector corresponding to
 * that component (text/number fields selected as numerator & denominator).
 */
function _webform_percentcalc_convert_key_to_input($component_key, $extra = '') {
  return sprintf('input[name*="%s"]', $component_key);
}

/**
 * Convert a component_key into a jquery selector for the hidden field.
 */
function _webform_percentcalc_convert_key_to_hidden($component_key) {
  return sprintf('input[name="[%s][hidden]"]', $component_key);
}

/**
 * Convert component_key into a jquery selector for the percentcalc label div.
 */
function _webform_percentcalc_convert_key_to_percentcalc_label($component_key) {
  $component_id = strtr($component_key, '_', '-');
  return sprintf('div[class*="%s"]', $component_id);
}
