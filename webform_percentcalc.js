(function ($) {
// Basic D7 jquery setup: http://drupal.org/node/756722

 // webform_percentcalc's code begins here
 Drupal.webform_percentcalc = Drupal.webform_percentcalc || {};
 Drupal.webform_percentcalc.isNumeric = function(num) {
   if (typeof $.isNumeric == 'function') {
     return $.isNumeric(num);
   } else {
     return !isNaN(parseFloat(num)) && isFinite(num);
   }
 }
 Drupal.webform_percentcalc.calculate = function(changedElement, percent, hidden, num, den, precision, suffix, undefined_msg, calculation) {
   // Get the numerator and denominator values, remove thousands separator.
//   if (typeof $(num).val()  !== "") { var num_v  = $(num).val().replace(/(\d+),(?=\d{3}(\D|$))/g, "$1");  } else { var num_v  = $(num).val();  }
//   if (typeof $(den).val()  !== "") { var den_v  = $(den).val().replace(/(\d+),(?=\d{3}(\D|$))/g, "$1");  } else { var den_v  = $(den).val();  }




   var num_v = $(num).val().replace(/(\d+),(?=\d{3}(\D|$))/g, "$1");
   var den_v = $(den).val().replace(/(\d+),(?=\d{3}(\D|$))/g, "$1");

   if ((Drupal.webform_percentcalc.isNumeric(num_v)) &&
       (Drupal.webform_percentcalc.isNumeric(den_v))) {
     if (den_v > 0) {
       switch (calculation) {
         case 'percentage':
           percent_v = (num_v / den_v * 100).toFixed(precision);
           if (percent_v > 100 && changedElement != '') {
             alert("The numerator must be less than or equal to the denominator.");
             // Clear the input value of the changed element (either the num or
             // the denom field).
             $(changedElement).val('');
             percent_v = '';
           }
           break;
         case 'division':
           percent_v = (num_v / den_v).toFixed(precision);
           break;
       }
       $(percent+" span.webform-percentcalc-value").text(percent_v);
       $(percent+" span.field-suffix").text(suffix);
       $(hidden).val(percent_v);
     }
     if (den_v == 0) {
       $(percent+" span.webform-percentcalc-value").text(undefined_msg);
       $(percent+" span.field-suffix").text("");
       $(hidden).val();
     }
   } else {
     $(percent+" span.webform-percentcalc-value").text("");
     $(hidden).val();
   }
 };

 // This is the entire module's custom extension of the Drupal.behaviors object
 Drupal.behaviors.webformPercentcalc = {
   // This is the attach function, replacing DocumentReady
   attach: function (context, settings) {

     // Loop through the sets of calculationIDs (which are really just cids of components in the webform)
     for (componentID in settings.webform_percentcalc) {

       // Collect drupal settings passed in from PHP
       var eventData = {
         percent: settings.webform_percentcalc[componentID].percentage,
         hidden: settings.webform_percentcalc[componentID].hidden_value,
         numerator: settings.webform_percentcalc[componentID].numerator,
         denominator: settings.webform_percentcalc[componentID].denominator,
         precision: settings.webform_percentcalc[componentID].precision,
         suffix: settings.webform_percentcalc[componentID].suffix,
         undefined_msg: settings.webform_percentcalc[componentID].undefined_msg,
         calculation: settings.webform_percentcalc[componentID].calculation
       }
       // Run first when page loads (in case of an error or reload)
       Drupal.webform_percentcalc.calculate(
         '',
         eventData.percent, eventData.hidden, eventData.numerator,
         eventData.denominator, eventData.precision, eventData.suffix,
         eventData.undefined_msg, eventData.calculation);

       // attach change() functions to each input, using defined calculation function
       $(eventData.numerator, context).change(eventData, function(event) {
         Drupal.webform_percentcalc.calculate(
           eventData.numerator,
           event.data.percent, event.data.hidden, event.data.numerator,
           event.data.denominator, event.data.precision, event.data.suffix,
           event.data.undefined_msg, event.data.calculation);
       });
       $(eventData.denominator, context).change(eventData, function(event) {
         Drupal.webform_percentcalc.calculate(
           eventData.denominator,
           event.data.percent, event.data.hidden, event.data.numerator,
           event.data.denominator, event.data.precision, event.data.suffix,
           event.data.undefined_msg, event.data.calculation);
       });

       //TODO: add some styling and theming hooks so label can be configured and display can be easily tweaked
       $(eventData.percent).show();
     }
   }
 };
// Assign D7's jQuery object to the $ variable, within the enclosure'd function on line 1
})(jQuery); 
